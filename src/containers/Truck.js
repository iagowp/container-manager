import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Container from '../components/container';

const flexContainerStyle = {
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'space-around',
  alignContent: 'space-between',
  height: '85vh',
};

class Truck extends PureComponent {
  render() {
    const { containers } = this.props;
    return (
      <div style={flexContainerStyle}>
        {containers.map(container => (
          <Container key={container.name} containerData={container} />
        ))}
      </div>
    );
  }
}

Truck.propTypes = {
  containers: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({ containers: state.containers });

export default connect(mapStateToProps)(Truck);
