import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-modal';
import Management from '../components/container-management';
import { toggleModal, openDoor } from '../actions';

if (process.env.NODE_ENV !== 'test') Modal.setAppElement('#root');

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

const menuStyle = {
  height: '10vh',
  textAlign: 'center',
  margin: '2vh 0',
};

class Menu extends Component {
  openDoor = () => {
    const { dispatch } = this.props;
    dispatch(openDoor());
  }

  toggleModal = () => {
    const { dispatch } = this.props;
    dispatch(toggleModal());
  }

  render() {
    const { isModalOpen, containers } = this.props;

    return (
      <div className="menu" style={menuStyle}>
        <button type="button" onClick={this.toggleModal}>
          Manage containers
        </button>
        <Modal
          isOpen={isModalOpen}
          style={customStyles}
          onRequestClose={this.toggleModal}
        >
          <button type="button" onClick={this.openDoor}>
            Open Door (Increase temperatures of all containers 2°)
          </button>
          <h3>Manage containers</h3>
          {containers.map(container => (
            <Management key={container.name} containerData={container} />
          ))}
        </Modal>
      </div>
    );
  }
}

Menu.propTypes = {
  isModalOpen: PropTypes.bool.isRequired,
  containers: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => (
  {
    isModalOpen: state.isModalOpen,
    containers: state.containers,
  }
);

export default connect(mapStateToProps)(Menu);
