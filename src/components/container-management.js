import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { setTemperature, emptyContainer, setContainer } from '../actions';

const style = {
  border: '1px solid black',
  padding: '5px',
};

class Management extends Component {
  constructor(props) {
    super(props);

    this.state = {
      temperature: 0,
      beerType: 'Pilsner',
    };
  }

  emptyContainer = (e) => {
    e.preventDefault();
    const { dispatch, containerData } = this.props;
    dispatch(emptyContainer(containerData.name));
  }

  setTemperature = (e) => {
    e.preventDefault();
    const { value } = e.target;
    this.setState(state => ({
      ...state,
      temperature: value,
    }));
  }

  submitTemperature = (e) => {
    e.preventDefault();
    const { dispatch, containerData } = this.props;
    const { temperature } = this.state;
    dispatch(setTemperature(containerData.name, temperature));
  }

  setBeerType = (e) => {
    e.preventDefault();
    const { value } = e.target;
    this.setState(state => ({
      ...state,
      beerType: value,
    }));
  }

  setContainer = (e) => {
    e.preventDefault();
    const { dispatch, containerData } = this.props;
    const { beerType } = this.state;
    dispatch(setContainer(containerData.name, beerType));
  }

  renderFilledContainer = () => (
    <div>
      <button type="button" onClick={this.emptyContainer}>
        Empty container
      </button>
    </div>
  )

  renderEmptyContainer = () => {
    const { beerTypes } = this.props;
    const { beerType } = this.state;
    return (
      <div>
        <select value={beerType} onChange={this.setBeerType}>
          {_.map(beerTypes, beerTypeOption => (
            <option key={beerTypeOption.name} value={beerTypeOption.name}>
              {beerTypeOption.name}
            </option>
          ))}
        </select>
        <button type="button" onClick={this.setContainer}>
          Fill container
        </button>
      </div>
    );
  }

  render() {
    const { containerData: container } = this.props;
    const { temperature } = this.state;

    return (
      <div style={style}>
        <h3 style={{ textAlign: 'center', margin: 6 }}>
          {container.name}
        </h3>
        <div>
          <input type="number" value={temperature} onChange={this.setTemperature} />
          <button type="button" style={{ margin: 3 }} onClick={this.submitTemperature}>
            Set temperature
          </button>
        </div>
        {container.beerType
          ? this.renderFilledContainer()
          : this.renderEmptyContainer()
        }
      </div>
    );
  }
}

Management.propTypes = {
  containerData: PropTypes.object.isRequired,
  beerTypes: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};


const mapStateToProps = state => ({ beerTypes: state.beerTypes });
export default connect(mapStateToProps)(Management);
