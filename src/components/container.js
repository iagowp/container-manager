import React from 'react';
import PropTypes from 'prop-types';

const containerStyle = {
  width: '30%',
  textAlign: 'center',
  height: '40vh',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-evenly',
  fontWeight: 'bold',
};

class Container extends React.PureComponent {
  render() {
    const { containerData } = this.props;

    if (!containerData.beerType) {
      return (
        <div style={containerStyle}>
          {`${containerData.name} is Empty`}
        </div>
      );
    }

    return (
      <div
        style={containerStyle}
        className={containerData.temperatureIsInRange ? 'temperature-in-range' : 'temperature-not-in-range'}
      >
        {containerData.name}
        <div>
          {`Beer Type: ${containerData.beerType}`}
        </div>
        <div>
          {`Temperature: ${containerData.temperature}°C`}
        </div>
      </div>
    );
  }
}

Container.propTypes = {
  containerData: PropTypes.object.isRequired,
};

export default Container;
