import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Container from '../components/container';

Enzyme.configure({ adapter: new Adapter() });

const emptyContainer = { name: 'container 1' };

const inRangeContainer = {
  name: 'container 2',
  beerType: 'IPA',
  temperature: 5,
  temperatureIsInRange: true,
};

const outOfRangeContainer = {
  name: 'container 3',
  beerType: 'Lager',
  temperature: 12,
  temperatureIsInRange: false,
};

it('should render without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Container containerData={emptyContainer} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('should render empty containers', () => {
  const wrapper = shallow(<Container containerData={emptyContainer} />);
  expect(wrapper.find('div').length).toBe(1);
  expect(wrapper.text()).toBe('container 1 is Empty');
});

it('should render populated container', () => {
  const wrapper = shallow(<Container containerData={inRangeContainer} />);
  expect(wrapper.find('div').length).toBe(3);
});

it('should render container class for in range temperature', () => {
  const wrapper = shallow(<Container containerData={inRangeContainer} />);
  expect(wrapper.find('.temperature-in-range').length).toBe(1);
});

it('should render container class for out of range temperature', () => {
  const wrapper = shallow(<Container containerData={outOfRangeContainer} />);
  expect(wrapper.find('.temperature-not-in-range').length).toBe(1);
});
