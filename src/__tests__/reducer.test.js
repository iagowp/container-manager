import reducer from '../reducers';
import * as actions from '../actions';

const initialStateContainers = [
  { name: 'Container 1' },
  { name: 'Container 2' },
  { name: 'Container 3' },
  { name: 'Container 4' },
  { name: 'Container 5' },
  { name: 'Container 6' },
];

it('should be a function', () => {
  expect(reducer).toBeInstanceOf(Function);
});

it('actions should be an object', () => {
  expect(typeof actions).toBe('object');
});

it('should return a state with containers and isModalOpen properties', () => {
  const newState = reducer(undefined, {});
  expect(newState.containers).toEqual(initialStateContainers);
  expect(newState.isModalOpen).toBe(false);
});

it('should set a container', () => {
  const action = actions.setContainer('Container 1', 'Lager');
  const newState = reducer(undefined, action);
  expect(newState.containers[0]).toEqual({
    name: 'Container 1',
    beerType: 'Lager',
    temperature: 20,
    temperatureIsInRange: false,
  });
});

it('should set a container temperature', () => {
  let newState = reducer(undefined, actions.setContainer('Container 2', 'Lager'));
  newState = reducer(newState, actions.setTemperature('Container 2', 5));
  expect(newState.containers[1]).toEqual({
    name: 'Container 2',
    beerType: 'Lager',
    temperature: 5,
    temperatureIsInRange: true,
  });
});

it('should increase temperatures on all containers when door is opened', () => {
  const containersWithTemperatures = initialStateContainers.map((container, i) => ({
    ...container,
    temperature: i,
  }));
  const newState = reducer({ containers: containersWithTemperatures }, actions.openDoor());
  expect(newState.containers[0].temperature).toEqual(2);
  expect(newState.containers[1].temperature).toEqual(3);
  expect(newState.containers[2].temperature).toEqual(4);
  expect(newState.containers[3].temperature).toEqual(5);
  expect(newState.containers[4].temperature).toEqual(6);
  expect(newState.containers[5].temperature).toEqual(7);
});

it('should empty container', () => {
  let newState = reducer(undefined, actions.setContainer('Container 4', 'IPA'));
  newState = reducer(newState, actions.setTemperature('Container 4', 5));
  newState = reducer(newState, actions.emptyContainer('Container 4'));
  expect(newState.containers[3].temperature).toBeUndefined();
  expect(newState.containers[3].beerType).toBeUndefined();
  expect(newState.containers[3].temperatureIsInRange).toBeUndefined();
});

it('should open modal', () => {
  const newState = reducer(undefined, actions.toggleModal());
  expect(newState.isModalOpen).toBe(true);
});

it('should close modal', () => {
  let newState = reducer(undefined, actions.toggleModal());
  newState = reducer(newState, actions.toggleModal());
  expect(newState.isModalOpen).toBe(false);
});
