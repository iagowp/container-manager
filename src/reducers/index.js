import {
  TOGGLE_MODAL,
  SET_TEMPERATURE,
  EMPTY_CONTAINER,
  SET_CONTAINER,
  OPEN_DOOR,
} from '../actiontypes';

const beerTypes = {
  Pilsner: {
    name: 'Pilsner',
    temperatureRange: {
      min: 4,
      max: 6,
    },
  },
  IPA: {
    name: 'IPA',
    temperatureRange: {
      min: 5,
      max: 6,
    },
  },
  Lager: {
    name: 'Lager',
    temperatureRange: {
      min: 4,
      max: 7,
    },
  },
  Stout: {
    name: 'Stout',
    temperatureRange: {
      min: 6,
      max: 8,
    },
  },
  'Wheat Beer': {
    name: 'Wheat Beer',
    temperatureRange: {
      min: 5,
      max: 6,
    },
  },
  'Pale Ale': {
    name: 'Pale Ale',
    temperatureRange: {
      min: 5,
      max: 6,
    },
  },
};

const isTemperatureInRange = (temperature, beerType) => {
  if (!beerType) return undefined;
  const beerTypeObj = beerTypes[beerType];
  if (!beerTypeObj) return undefined;
  const isAboveMin = temperature >= beerTypeObj.temperatureRange.min;
  const isBelowMax = temperature <= beerTypeObj.temperatureRange.max;
  return isAboveMin && isBelowMax;
};

// ex populated container
// {name: 'containerN', beerType: 'IPA', temperature: 5, temperatureIsInRange: true}

const initialState = {
  beerTypes,
  containers: [
    {
      name: 'Container 1',
    },
    {
      name: 'Container 2',
    },
    {
      name: 'Container 3',
    },
    {
      name: 'Container 4',
    },
    {
      name: 'Container 5',
    },
    {
      name: 'Container 6',
    },
  ],
  isModalOpen: false,
};

export default function (state = initialState, action) {
  let temperature;
  let containerName;
  let containers;
  let index;
  let newContainer;
  let beerType;

  switch (action.type) {
    case TOGGLE_MODAL:
      return {
        ...state,
        isModalOpen: !state.isModalOpen,
      };

    case SET_TEMPERATURE:
      ({ temperature, containerName } = action.payload);
      containers = state.containers.slice();
      index = containers.findIndex(container => (
        container.name === containerName
      ));
      newContainer = {
        ...containers[index],
        temperature: action.payload.temperature,
        temperatureIsInRange: isTemperatureInRange(temperature, containers[index].beerType),
      };
      containers.splice(index, 1, newContainer);
      return {
        ...state,
        containers,
      };

    case EMPTY_CONTAINER:
      ({ containerName } = action.payload);
      containers = state.containers.slice();
      index = containers.findIndex(container => (
        container.name === containerName
      ));
      newContainer = { name: containers[index].name };
      containers.splice(index, 1, newContainer);
      return {
        ...state,
        containers,
      };

    case SET_CONTAINER:
      ({ containerName, beerType } = action.payload);
      containers = state.containers.slice();
      index = containers.findIndex(container => (
        container.name === containerName
      ));
      newContainer = {
        name: containers[index].name,
        beerType,
        temperature: 20,
        temperatureIsInRange: false,
      };
      containers.splice(index, 1, newContainer);
      return {
        ...state,
        containers,
      };

    case OPEN_DOOR:
      containers = state.containers.map((container) => {
        const newTemperature = Number(container.temperature) + 2 || undefined;

        return {
          ...container,
          temperature: newTemperature,
          temperatureIsInRange: isTemperatureInRange(newTemperature, container.beerType),
        };
      });
      return {
        ...state,
        containers,
      };

    default:
      return state;
  }
}
