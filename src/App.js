import React from 'react';
import './css/App.css';
import { Provider } from 'react-redux';
import Truck from './containers/Truck';
import store from './store';
import Menu from './containers/Menu';

const App = () => (
  <Provider store={store}>
    <div>
      <Menu />
      <Truck />
    </div>
  </Provider>
);

export default App;
