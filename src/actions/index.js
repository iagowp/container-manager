import {
  TOGGLE_MODAL,
  SET_TEMPERATURE,
  EMPTY_CONTAINER,
  SET_CONTAINER,
  OPEN_DOOR,
} from '../actiontypes';

export const toggleModal = () => ({ type: TOGGLE_MODAL });

export const setTemperature = (containerName, temperature) => ({
  type: SET_TEMPERATURE,
  payload: {
    containerName,
    temperature,
  },
});

export const emptyContainer = containerName => ({
  type: EMPTY_CONTAINER,
  payload: { containerName },
});

export const setContainer = (containerName, beerType) => ({
  type: SET_CONTAINER,
  payload: {
    containerName,
    beerType,
  },
});

export const openDoor = () => ({ type: OPEN_DOOR });
