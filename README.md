Some of the questions I'd ask before implementing this project, and the answers I gave myself:

**Q: Is there a maximum amount of containers the truck can fit?**

A: Yes, 6.


**Q: How should the data be displayed?**

A: Shane will keep a tablet on his truck, so a visualization fitting a tablet should be fine.


**Q: What kind of alert do you need?**

A: Having the container represented in red should be enough


**Q: Where do I get the beer types from?**

A: As we are not planning on adding any other beer anytime soon, hardcoding is ok.


**Q: What data format will I be working with?**

A: Up to you.


**Q: What should be the temperature in container starting to be used?**

A: 20°C


To run the project: run `yarn`, then `yarn run`. 
To run tests, `yarn test`

  

## What could have been done in a better way? What would I do in version 2.0?

  

Well, I didn't put too much time at making the app look good, that is definitely an area that could be improved a lot, along with UX.

A 2.0 version would probably have a better organized reducer.

The `src` folder has too many files, as the project grows, some of them would need to move, such as app and store.

The styling on the project should also be more standardized, deciding specific rules for when to do inline styles, and when to do it on css file would be a important step forward.
Talking about css, I'd move the little code I have on css to sass.

On testing, the next step would be to test reducers more extensively, checking for edge cases, and add more rendering tests.

On functionality, it this were to be kept as frontend only, I'd improve the 'open door' function, and add a way for all containers to have its temperature decreased. I'd also keep the temperature input in sync with redux.

  
  

## Code organization

**__ tests__**
> Folder for test related files

**Actions**
> Where my redux action creators are store, this is the place I'd add api calls, for example.

**Actiontypes**
> Const file with string names for action types, used by both actions and reducer files

**Components**
> Reusable components for code abstraction

**Containers**
> Bigger elements needing to connect to redux store and pass along data/call actions

**Css**
 >Where my css code stays

**Reducers**
> Files that will handle app state
